variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "francis.gravador@yahoo.com"
}

variable "image_id_filter" {
  default = "amzn2-ami-hvm-2.0.*-x86_64-gp2"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "db_username" {
  description = "Username fo the RDS PostGres instance"
}

variable "db_password" {
  description = "Password for the RDS PostGres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "570252958048.dkr.ecr.ap-southeast-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "570252958048.dkr.ecr.ap-southeast-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "domain-name-registered-in-aws-route-53 (mightypenny.net)"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    productino = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}